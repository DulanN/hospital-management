<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function authenticated(Request $request, $user)
    {

            $user = Auth::id();
            $role = DB::table('model_has_roles')
                ->select('roles.name')
                ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->where('model_has_roles.model_id', '=', $user)
                ->get();

            if ($role[0]->name == 'admin') {

                return redirect('/sales');
            }
            else if($role[0]->name == 'doctor')
            {
                return redirect('/doctor/doctor_dashboard');
            }
            else if($role[0]->name == 'receptionist')
            {
                return redirect('/receptionist/receptionist_dashboard');
            }
            else if($role[0]->name == 'client')
            {
                return redirect('/client/analytics');
            }
            else{
                return redirect('/user/welcome');
            }

    }
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
