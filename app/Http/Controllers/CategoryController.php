<?php

namespace App\Http\Controllers;

use App\categories;
use App\category_has_services;
use App\service_addons;
use App\services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class CategoryController extends Controller
{
    public function add_services(Request $request)
    {
        $data = [
            'category_name' => 'Services',
            'page_name' => 'Add Services',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',

        ];

        $categories = DB::table('categories')
            ->select('categories.category_name', 'categories.id')
            ->where('categories.status','=',1)
            ->get();

        $services = DB::table('categories')
            ->select('categories.category_name', 'services.service_name', 'categories.id', 'services.id as ser_id','services.price','services.cost')
            ->leftJoin('category_has_services', 'categories.id', '=', 'category_has_services.category_id')
            ->leftJoin('services', 'category_has_services.service_id', '=', 'services.id')
            ->where('services.status','=',1)
            ->where('categories.status','=',1)
//            ->groupBy('categories.category_name')
            ->get();



        //dd($categories,$services);
        return view('admin.services.add_services', compact('categories', 'services'))->with($data);
    }
    public function change_sort($ser_id, $cat_id)
    {
        DB::table('category_has_services')->whereservice_id($ser_id)->update(array('category_id' => $cat_id));
    }

    public function add_addons()
    {
        $data = [
            'category_name' => 'Services',
            'page_name' => 'Add Addons',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',

        ];

        $categories = DB::table('categories')
            ->select('categories.category_name', 'categories.id')
            ->where('categories.status','=',1)
            ->get();

        $addons = DB::table('categories')
            ->select('categories.category_name', 'service_addons.addon_name', 'categories.id', 'service_addons.id as addon_id','service_addons.price','service_addons.cost')
            ->leftJoin('category_has_addons', 'categories.id', '=', 'category_has_addons.category_id')
            ->leftJoin('service_addons', 'category_has_addons.addon_id', '=', 'service_addons.id')
            ->where('service_addons.status','=',1)
            ->where('categories.status','=',1)
//            ->groupBy('categories.category_name')
            ->get();

        dd($addons);

        return view('admin.services.add_services', compact('categories', 'services'))->with($data);

    }
    public function category_add(Request $request)
    {

        if ($request->cat_edit_type == 'edit')

        {
            $id = $request->cat_edit_id;
            categories::find($id)->update(['category_name' => $request->Category]);

            return redirect('/add_services');
        }

        else
        {
            $name = $request->Category;
            $category = new categories();
            $category->category_name = $name;
            $category->save();

            return redirect('/add_services');
        }


    }

    public function delete_service(Request $request)
    {
        $id = $request->delete;

        services::find($id)->update(['status' => '0']);

        return redirect('/add_services');
    }

    public function create_service(Request $request)
    {

        $request->validate([

            'Name' => 'required',
            'Cost' => 'required|numeric',
            'Price' => 'required|numeric',

        ]);

        if ($request->type == 'edit') {


            $id = $request->id;
            $name = $request->Name;
            $cost = $request->Cost;
            $price = $request->Price;

            services::find($id)->update(['service_name' => $name]);
            services::find($id)->update(['cost' => $cost]);
            services::find($id)->update(['price' => $price]);

            return redirect()->back();


        }

        else {
            $id = $request->id;
            $name = $request->Name;
            $cost = $request->Cost;
            $price = $request->Price;

            $service = new services();
            $service->service_name = $name;
            $service->price = $price;
            $service->cost = $cost;
            $service->save();

            $ser_id = DB::table('services')->latest('id')->first();


            $category_has_services = new category_has_services();
            $category_has_services->category_id = $id;
            $category_has_services->service_id = $ser_id->id;
            $category_has_services->save();


            return redirect('/add_services');
        }

    }

    public function doctors(Request $request)
    {
        $doctors = User::role('doctor')->get();

        $data = [
            'category_name' => 'Services',
            'page_name' => 'Doctors',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'doctors'=>$doctors,
        ];


        $services = DB::table('doctors')
            ->select('doctors.doctor_name', 'services.service_name', 'doctors.id', 'services.id as ser_id')
            ->leftJoin('doctor_make_services', 'doctors.id', '=', 'doctor_make_services.doctor_id')
            ->leftJoin('services', 'doctor_make_services.service_id', '=', 'services.id')
//            ->groupBy('categories.category_name')
            ->get();

        //dd($categories,$services);
        return view('admin.services.doctors', compact('doctors', 'services'))->with($data);

    }

}
