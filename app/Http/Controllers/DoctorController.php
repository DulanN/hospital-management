<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DoctorController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ]);

        $input['password'] = Hash::make('password');
        $input['user_id'] =  Str::random(9);
        $input['email'] = $request->email ;
        $input['name'] =  $request->name;
        $input['surname'] =  $request->surname;
        $input['mobile_no'] =  $request->mobile_no;


        $user = User::create($input);
        $user->assignRole('doctor');

       return redirect()->back();

    }
    public function edit_doctor($id)
    {
        $doctor_data=User::whereid($id)->first();
        $data = [
            'category_name' => 'users',
            'page_name' => 'account_settings',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'doctor'=>$doctor_data,
        ];

        return view('doctor.edit_doctor')->with($data);

    }

}
