<?php

namespace App\Http\Controllers;

use App\categories;
use App\invoice_has_addons;
use App\invoice_has_services;
use App\invoice_table;
use App\services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class InvoiceController extends Controller
{
    public function index()
    {
        $data = [
            'category_name' => 'Contracts',
            'page_name' => 'Contracts',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',

        ];

       $invoice_services = DB::table('invoice_has_services')
           ->select('invoice_has_services.*','services.service_name','services.price')
           ->leftjoin('services','services.id','=','invoice_has_services.service_id')
           ->get();


        $invoice_addons = DB::table('invoice_has_addons')
            ->select('invoice_has_addons.*','service_addons.addon_name','service_addons.price')
            ->leftjoin('service_addons','service_addons.id','=','invoice_has_addons.addon_id')
            ->get();

       $invoice_data = DB::table('invoice_tables')
           ->select('invoice_tables.*','users.name','doctors.doctor_name')
           ->leftJoin('users','users.id','=','invoice_tables.client_id')
           ->leftJoin('doctors','doctors.id','=','invoice_tables.doctor_id')
           ->get();



        return view('admin.contracts.contracts',compact('invoice_data','invoice_services','invoice_addons'))->with($data);
    }

    public function add_new_invoice()
    {
        $data = [
            'category_name' => 'Contracts',
            'page_name' => 'Contracts',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'clients'=> User::role('client')->get(),
            'doctors'=>User::role('doctor')->get(),
            'services'=>DB::table('services')->get(),
            'addons'=>DB::table('service_addons')->get(),
        ];

        return view('admin.contracts.add_new')->with($data);
    }


    public function submit_new_invoice(Request $request)
    {

        $latest = invoice_table::latest()->first();

        if($latest == null)
        {
            $latest->invoice_number = 'inv000';
            //$invoice->invoice_number = 'inv'.++$latest->invoice_number;
        }

        $name = $request->client_name;
        $invoice = new invoice_table();
        $invoice->invoice_number = ++$latest->invoice_number;
        $invoice->client_id = $request->client_name;
        $invoice->doctor_id = $request->doctor;
        $invoice->deposit_amount = $request->deposit_amount;
        $invoice->discount_rate = $request->discount_percentage;
        $invoice->special_note = $request->note;

        $count_ser = count($request->services);


        $subtotal_service = 0;
        $subtotal_addon = 0;

        for($i =0 ; $i<$count_ser ; $i++)
        {
            $service_cost = DB::table('services')
                ->select('services.price')
                ->where('services.id','=',$request->services[$i])
                ->value('price');

//            $services = new invoice_has_services();
//            $services->invoice_id = $invoice->invoice_number;
//            $services->service_id = $request->services[$i];
//            $services->save();

            $subtotal_service+=$service_cost;
        }

        if(isset($request->add_ons))
        {
            $count_addon = count($request->add_ons);

            for($i =0 ; $i<$count_addon ; $i++)
            {
                $addon_cost = DB::table('service_addons')
                    ->select('service_addons.price')
                    ->where('service_addons.id','=',$request->add_ons[$i])
                    ->value('price');
                $subtotal_addon+=$addon_cost;
            }
        }


        $subtotal = $subtotal_service+$subtotal_addon;

        if(isset($request->discount_percentage))
        {
            $invoice->discount_rate = $request->discount_percentage;
            $invoice->discount_ammount =  $subtotal*$request->discount_percentage/100;

            $invoice->grand_total = ($subtotal -$invoice->discount_ammount) - $invoice->deposit_amount ;
        }
        else{
            $invoice->discount_rate = 0;
            $invoice->discount_ammount = 0;

            $invoice->grand_total = ($subtotal - $invoice->discount_ammount) - $invoice->deposit_amount ;
        }


        $invoice->subtotal = $subtotal;
        $invoice->save();

        $latest_inv = invoice_table::latest()->first();

        for($i =0 ; $i<$count_ser ; $i++)
        {
            $services = new invoice_has_services();
            $services->invoice_id = $latest_inv->id;
            $services->service_id = $request->services[$i];
            $services->save();
        }

        if(isset($request->add_ons))
        {
            for($i =0 ; $i<$count_addon ; $i++) {
                $services = new invoice_has_addons();
                $services->invoice_id = $latest_inv->id;
                $services->addon_id = $request->add_ons[$i];
                $services->save();
            }
        }


    }
}
