<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::id();
            $role = DB::table('model_has_roles')
                ->select('roles.name')
                ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->where('model_has_roles.model_id', '=', $user)
                ->get();

            if ($role[0]->name == 'admin') {

                return redirect('/sales');
            }
            else if($role[0]->name == 'Doctor')
            {
                
                return redirect('/doctor/doctor_dashboard');
            }
            else{
                return $next($request);
            }
        }
        return $next($request);
    }
}
