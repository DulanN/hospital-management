<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_tables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->string('invoice_number');
            $table->date('invoice_date');
            $table->date('invoice_due_date');
            //$table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('doctor_id')->nullable();
//            $table->unsignedBigInteger('service_id')->nullable();
//            $table->unsignedBigInteger('addon_id')->nullable();
            $table->integer('deposit_amount');
            $table->integer('subtotal');
            $table->integer('grand_total');
//            $table->integer('tax_amount');
            $table->integer('discount_rate');
            $table->integer('discount_ammount');
            $table->text('special_note');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('doctor_id')->references('doc_id')->on('doctors')->onDelete('SET NULL');
//            $table->foreign('service_id')->references('id')->on('services')->onDelete('SET NULL');
//            $table->foreign('addon_id')->references('id')->on('service_addons')->onDelete('SET NULL');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_tables');
    }
}
