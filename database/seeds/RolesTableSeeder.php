<?php

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::truncate();

        $permission = Permission::all();

        $roleAdmin = Role::create(['name' => 'admin']);
        Role::create(['name' => 'client']);
        Role::create(['name' => 'receptionist']);
        Role::create(['name' => 'doctor']);

        $roleAdmin->syncPermissions($permission);
    }
}
