<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::insert([
        	[
        		'name'		=> 'admin',
        		'user_id'   => 'abs',
        		'email'		=> 'admin@cork.com',
        		'surname'   => 'ayya',
        		'mobile_no' => '0772141220',
        		'act_camp_resp' => 'done',
        		'password'	=> bcrypt('password'),
			'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	],
        	[
        		'name'		=> 'Talha',
        		'email'		=> 'talha@wedevs.com',
                'user_id'   => 'abs',
                'surname'   => 'ayya',
                'mobile_no' => '0772141220',
                'act_camp_resp' => 'done',
        		'password'	=> bcrypt('weDevs'),
        		'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	],
        	[
        		'name'		=> 'Sadek',
        		'email'		=> 'sadek@wedevs.com',
                'user_id'   => 'abs',
                'surname'   => 'ayya',
                'mobile_no' => '0772141220',
                'act_camp_resp' => 'done',
        		'password'	=> bcrypt('weDevs'),
        		'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	],
        	[
        		'name'		=> 'Ashik',
        		'email'		=> 'ashik@wedevs.com',
                'user_id'   => 'abs',
                'surname'   => 'ayya',
                'mobile_no' => '0772141220',
                'act_camp_resp' => 'done',
        		'password'	=> bcrypt('weDevs'),
        		'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	],
        	[
        		'name'		=> 'Mahbub',
        		'email'		=> 'mahbub@wedevs.com',
                'user_id'   => 'abs',
                'surname'   => 'ayya',
                'mobile_no' => '0772141220',
                'act_camp_resp' => 'done',
        		'password'	=> bcrypt('weDevs'), 
        		'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	],
        	[
        		'name'		=> 'Rana',
        		'email'		=> 'rana@wedevs.com',
                'user_id'   => 'abs',
                'surname'   => 'ayya',
                'mobile_no' => '0772141220',
                'act_camp_resp' => 'done',
        		'password'	=> bcrypt('weDevs'),
        		'created_at'=> Carbon::now(),
        		'updated_at'=> Carbon::now()
        	]
    	]);
    }
}
