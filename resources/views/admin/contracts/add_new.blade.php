@extends('layouts.app')

@section('content')
    <div class="container-fluid col-6">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Add new invoice</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="/Contracts/submit_new_invoice" method="POST">
                    @csrf
                    <div class="form-group mb-4">
                        <label for="exampleFormControlInput2">Client Name</label>
                        <select class="form-control  basic" name="client_name" required>
                            <option selected="selected">Choose client</option>
                            @foreach($clients as $client)
                            <option value={{$client->id}}>{{$client->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlInput2">Client Location</label>
                        <select class="form-control  basic" name="client_location">
                            <option selected="selected">Choose client location</option>
                            <option>white</option>
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlInput2">Select doctor</label>
                        <select class="form-control  basic" name="doctor" required>
                            <option selected="selected">Choose doctor</option>
                            @foreach($doctors as $doctor)
                            <option value={{$doctor->doc_id}}>{{$doctor->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlInput2">Select services</label>
                        <select class="form-control tagging" name="services[]" multiple="multiple" required>

                            @foreach($services as $service)
                            <option value={{$service->id}}>{{$service->service_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlInput2">Add ons</label>
                        <select class="form-control  tagging" multiple="multiple" name="add_ons[]">

                            @foreach($addons as $addon)
                            <option value={{$addon->id}}>{{$addon->addon_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-4 mt-3">
                        <label for="">Deposit amount</label>
                        <input type="number" class="form-control" name="deposit_amount" required>
                    </div>
                    <div class="form-group mb-4 mt-3">
                        <label for="">Discount percentage</label>
                        <input type="number" value="0" class="form-control" name="discount_percentage">
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Add note</label>
                        <textarea class="form-control" name="note" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <input type="submit" class="mt-4 mb-4 btn btn-primary">
                </form>
            </div>
        </div>
    </div>

@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".tagging").select2({
            tags: true
        });
    });
</script>

