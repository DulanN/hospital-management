@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="container">
            <div class="row">
                <div id="mapAfrica" class="col-lg-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-header">
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>United Kingdom</h4>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content widget-content-area">
                            <div id="uk-map" style="height: 600px"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection