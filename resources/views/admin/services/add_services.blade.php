@extends('layouts.app')

@section('content')

    <div class="layout-px-spacing">
        <div class="action-btn layout-top-spacing mb-5">
            <p>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-grid">
                    <rect x="3" y="3" width="7" height="7"></rect>
                    <rect x="14" y="3" width="7" height="7"></rect>
                    <rect x="14" y="14" width="7" height="7"></rect>
                    <rect x="3" y="14" width="7" height="7"></rect>
                </svg>
                Boards
            </p>
            <button id="add-list" class="btn btn-primary">Add New Categoruy</button>
        </div>


        <div class="modal fade" id="addListModal" tabindex="-1" role="dialog" aria-labelledby="addListModalTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form action="/category_add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="compose-box">
                                <div class="compose-content" id="addListModalTitle">
                                    <h5 class="add-list-title">Add Category</h5>
                                    <h5 class="edit-list-title">Edit Category</h5>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="list-title">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                     stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                     class="feather feather-list">
                                                    <line x1="8" y1="6" x2="21" y2="6"></line>
                                                    <line x1="8" y1="12" x2="21" y2="12"></line>
                                                    <line x1="8" y1="18" x2="21" y2="18"></line>
                                                    <line x1="3" y1="6" x2="3" y2="6"></line>
                                                    <line x1="3" y1="12" x2="3" y2="12"></line>
                                                    <line x1="3" y1="18" x2="3" y2="18"></line>
                                                </svg>
                                                <input type="text" placeholder="Add Category"
                                                       class="form-control" id="Category" name="Category">

                                                <input type="hidden" name="cat_edit_type" id="cat_edit_type">
                                                <input type="hidden" name="cat_edit_id" id="cat_edit_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-x">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                                Discard
                            </button>
                            <button type="submit" class="btn edit-list" >Save</button>
                            <button type="submit" class="btn add-list">Add Category</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="deleteConformation" tabindex="-1" role="dialog"
             aria-labelledby="deleteConformationLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" id="deleteConformationLabel">
                    <div class="modal-header">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-trash-2">
                                <polyline points="3 6 5 6 21 6"></polyline>
                                <path
                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                <line x1="14" y1="11" x2="14" y2="17"></line>
                            </svg>
                        </div>
                        <h5 class="modal-title" id="exampleModalLabel">Delete the task?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="">If you delete the task it will be gone forever. Are you sure you want to
                            proceed?</p>
                    </div>

                    <form action="/delete_service" method="POST">
                        @csrf
                        <input type="hidden" name="delete" id="delete">
                        <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>

        <div class="row scrumboard" id="cancel-row">
            <div class="col-lg-12 layout-spacing">
                <div class="row">
                    @foreach($categories as $cat)
                        <div class="col-md-4 mt-3">
                            <div class="task-list-section">
                                <div data-section="s-new" class="task-list-container" data-connect="sorting">
                                    <div class="connect-sorting">
                                        <div class="task-container-header">
                                            <h6 class="s-heading"
                                                data-listTitle="In Progress">{{$cat->category_name}}</h6>
                                            <div class="dropdown">
                                                <a class="dropdown-toggle" href="#" role="button"
                                                   id="dropdownMenuLink-1"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2"
                                                         stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-more-horizontal">
                                                        <circle cx="12" cy="12" r="1"></circle>
                                                        <circle cx="19" cy="12" r="1"></circle>
                                                        <circle cx="5" cy="12" r="1"></circle>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right"
                                                     aria-labelledby="dropdownMenuLink-1">
                                                    <a onclick="edit_cat('{{$cat->id}}')" class="dropdown-item list-edit"
                                                       href="javascript:void(0);">Edit</a>
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($services as $ser)
                                            @if($ser->id == $cat->id )
                                                @if(isset($ser->service_name))
                                                    <div class="connect-sorting-content" id="{{$cat->id}}" data-sortable="true">
                                                        <div data-draggable="true" id="{{$ser->ser_id}}" class="card simple-title-task"
                                                             style="">
                                                            <div class="card-body">
                                                                <div class="task-header">
                                                                    <div class="">
                                                                        <h4 class=""
                                                                            data-taskTitle="Singapore Team Meet">{{$ser->service_name}}</h4>
                                                                    </div>
                                                                    <div class="">
                                                                        <a onclick="edit_service('{{$ser->ser_id}}','{{$ser->service_name}}' , '{{$ser->price}}', '{{$ser->cost}}') "  style="background: #191e3a;
                                                                            border: none;"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                     width="24"
                                                                                     height="24"
                                                                                     viewBox="0 0 24 24" fill="none"
                                                                                     stroke="currentColor"
                                                                                     stroke-width="2" stroke-linecap="round"
                                                                                     stroke-linejoin="round"
                                                                                     class="feather feather-edit-2 s-task-edit">
                                                                                <path
                                                                                        d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                                                            </svg></a>

                                                                        <a onclick="delete_ser('{{$ser->ser_id}}') "  style="background: #191e3a;
                                                                            border: none;"> <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24"
                                                                             height="24"
                                                                             viewBox="0 0 24 24" fill="none"
                                                                             stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             class="feather feather-trash-2 s-task-delete">
                                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                                            <path
                                                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                                            <line x1="10" y1="11" x2="10"
                                                                                  y2="17"></line>
                                                                            <line x1="14" y1="11" x2="14"
                                                                                  y2="17"></line>
                                                                            </svg></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif

                                        @endforeach
                                        <div class="add-s-task">
                                            <a class="addTask" type="button" onClick="create_service('{{$cat->id}}')" data-toggle="modal" data-target="#addTaskModal">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     viewBox="0 0 24 24"
                                                     fill="none" stroke="currentColor" stroke-width="2"
                                                     stroke-linecap="round"
                                                     stroke-linejoin="round" class="feather feather-plus-circle">
                                                    <circle cx="12" cy="12" r="10"></circle>
                                                    <line x1="12" y1="8" x2="12" y2="16"></line>
                                                    <line x1="8" y1="12" x2="16" y2="12"></line>
                                                </svg>
                                                Add Task</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="addTaskModalTitle"
                     aria-hidden="true">
                    <form action="/create_service" method="POST">
                        @csrf
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="compose-box">
                                        <div class="compose-content" id="addTaskModalTitle">
                                            <h5 class="add-task-title">Add Service</h5>
                                            <h5 class="edit-task-title">Edit Service</h5>

                                            <div class="addTaskAccordion" id="add_task_accordion">

                                                <div class="card task-simple">
                                                    <div class="card-header" id="headingOne">
                                                        <div class="mb-0" data-toggle="collapse" role="navigation"
                                                             data-target="#collapseOne" aria-expanded="false"
                                                             aria-controls="collapseOne"> Name

                                                            <label id="lblname"></label>
                                                        </div>
                                                    </div>

                                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                         data-parent="#add_task_accordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="task-title mb-4">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                             height="24" viewBox="0 0 24 24" fill="none"
                                                                             stroke="currentColor" stroke-width="2"
                                                                             stroke-linecap="round" stroke-linejoin="round"
                                                                             class="feather feather-edit-3">
                                                                            <path d="M12 20h9"></path>
                                                                            <path
                                                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                                        </svg>
                                                                        <input id="Name" type="text" placeholder="Name"
                                                                               class="form-control" name="Name" required>

                                                                        <input id = "s_id" type="hidden" name="id">
                                                                        <input id = "type" type="hidden" name="type">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card task-text-progress">
                                                    <div class="card-header" id="headingTwo">
                                                        <div class="mb-0" data-toggle="collapse" role="navigation"
                                                             data-target="#collapseTwo" aria-expanded="false"
                                                             aria-controls="collapseTwo"> Price ($)
                                                        </div>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                                         data-parent="#add_task_accordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="task-title mb-4">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                             height="24" viewBox="0 0 24 24" fill="none"
                                                                             stroke="currentColor" stroke-width="2"
                                                                             stroke-linecap="round" stroke-linejoin="round"
                                                                             class="feather feather-edit-3">
                                                                            <path d="M12 20h9"></path>
                                                                            <path
                                                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                                        </svg>
                                                                        <input id="Price" type="text" placeholder="Price"
                                                                               class="form-control" name="Price" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card task-image">
                                                    <div class="card-header" id="headingThree">
                                                        <div class="mb-0" data-toggle="collapse" role="navigation"
                                                             data-target="#collapseThree" aria-expanded="false"
                                                             aria-controls="collapseThree"> Cost ($)
                                                        </div>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                                         data-parent="#add_task_accordion">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="task-title mb-4">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                             height="24" viewBox="0 0 24 24" fill="none"
                                                                             stroke="currentColor" stroke-width="2"
                                                                             stroke-linecap="round" stroke-linejoin="round"
                                                                             class="feather feather-edit-3">
                                                                            <path d="M12 20h9"></path>
                                                                            <path
                                                                                d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                                        </svg>
                                                                        <input id="Cost" type="text" placeholder="Cost"
                                                                               class="form-control" name="Cost" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-x">
                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                        </svg>
                                        Discard
                                    </button>
                                    <button type="submit" data-btnfn="addTask" class="btn add-tsk">Add Task</button>
                                    <button data-btnfn="editTask" class="btn edit-tsk" style="display: none;">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <script>

                        function edit_service(id,service_name,price,cost) {
                            document.getElementById("s_id").value = id;
                            document.getElementById('Name').value = service_name;
                            document.getElementById("type").value = 'edit';
                            document.getElementById("Price").value = price;
                            document.getElementById("Cost").value = cost;

                        }

                        function delete_ser(id) {
                            document.getElementById("delete").value = id;
                        }
                        function edit_cat(id) {
                            document.getElementById("cat_edit_id").value = id;
                            document.getElementById('cat_edit_type').value = 'edit';
                        }
                        function create_service(id) {

                            document.getElementById("s_id").value = id;

                        {{--console.log(id);--}}
                            {{--let pointid = $(this).attr("data-pointid");--}}
                            {{--$.ajax({--}}
                                {{--url: '/create_service' ,--}}
                                {{--type: 'post',--}}
                                {{--headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },--}}
                                {{--data: { id:pointid,"_token": "{{ csrf_token() }}" , c_id: id} ,--}}
                                {{--dataType: 'json',--}}

                                {{--success: function (data) {--}}
                                    {{--var boostdata = "";--}}
                                    {{--$('#full_name').html(data[0].full_name);--}}
                                    {{--$('#email').html(data[0].email);--}}
                                    {{--$('#address').html(data[0].address);--}}
                                    {{--$('#contact_no').html(data[0].contact_no);--}}
                                    {{--$('#name_en').html(data[0].name_en);--}}
                                    {{--$('#detail').html(data[0].detail);--}}

                                    {{--$.each(array.boost_data, function (i,item ) {--}}

                                        {{--if(item.is_active==null)--}}
                                        {{--{--}}
                                            {{--label = "label label-danger";--}}
                                            {{--status = "free";--}}
                                        {{--}--}}
                                        {{--else--}}
                                        {{--{--}}
                                            {{--label = "label label-success";--}}
                                            {{--status = "paid";--}}
                                        {{--}--}}

                                        {{--boostdata += '<tr>' +--}}
                                            {{--'<td>' + item.boost_type_name + '</td>' +--}}
                                            {{--'<td> <label class="'+label+'"> '+status+' </label> </td>' +--}}
                                            {{--'<td>' + item.start_date_time + '</td>' +--}}
                                            {{--'<td>' + item.end_date_time + '</td>' +--}}
                                            {{--'</tr>';--}}

                                        {{--$('#tbody').html(boostdata);--}}
                                    {{--});--}}

                                {{--}--}}

                            {{--});--}}

                        }
                    </script>
                </div>
            </div>
        </div>
    </div>

    </div>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dragula/3.6.1/dragula.min.js" integrity="sha512-kn16YS1EMScMKJGahSMt/OHDeafiU8k5ZNMBoW4eC4gTEUDT5v7JzvxM2z2kDYJq/lRuMZDjgwHwhSQ1jPy8Sw==" crossorigin="anonymous"></script>

<script>
    function change_sort(ser_id,cat_id){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "/change_sort/" + ser_id +"/"+ cat_id,
            success: function (results) {
              console.log(results);
            }
        });
    }
</script>
