@extends('layouts.app')

@section('content')

    <div class="layout-px-spacing">
        <div class="action-btn layout-top-spacing mb-5">
            <button id="add-list" data-toggle="modal" data-target="#add_doctor" class="btn btn-primary">Add New Doctor
            </button>
        </div>

        <div class="modal fade bd-example-modal-lg" id="add_doctor" tabindex="-1" role="dialog"
             aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myLargeModalLabel">Add new doctor</h5>
                    </div>
                    <div class="modal-body">
                        <form action="/add_new_doctor" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Name</label>
                                <input type="text" name="name" class="form-control" id="exampleFormControlInput1"
                                       placeholder="Doctor name">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Surname</label>
                                <input type="text" name="surname" class="form-control" id="exampleFormControlInput1"
                                       placeholder="Doctor surname">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Email</label>
                                <input type="email" name="email" class="form-control" id="exampleFormControlInput1"
                                       placeholder="Doctor email">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Mobile number</label>
                                <input type="text" name="mobile_no" class="form-control" id="exampleFormControlInput1"
                                       placeholder="Doctor Phone Number">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Doctor City</label>
                                <select class="form-control" name="city_id" id="exampleFormControlSelect1">
                                    <option>Choose</option>
                                    <option></option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                <button type="submit" class="btn btn-primary">Add new doctor</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @foreach($doctors as $doctor)
                    <div class="p-3 col-md-3 col-12">
                        <div class="card component-card_3">
                            <div class="card-body">
                                <img src="https://designreset.com/cork/ltr/demo3/assets/img/profile-7.jpeg" class="card-img-top" alt="...">
                                <h5 class="card-user_name">{{$doctor->name}} {{$doctor->surname}}</h5>
                                <p class="card-user_occupation">{{$doctor->email}}</p>
                                <p class="card-text">{{$doctor->mobile_no}}</p>
                            </div>
                            <div class="p-2">
                                <a class="btn btn-primary float-right" href="/edit_doctor/{{$doctor->id}}">Edit</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
