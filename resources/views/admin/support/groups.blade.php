@extends('layouts.app')

@section('content')
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <h6>Clients</h6>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <span class="badge badge-info">IN PROGRESS</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}"
                                                     class="ml-0">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item badge-notify mr-0">
                                                <div class="notification">
                                                    <a href="/support/ChatView"><span
                                                                class="badge badge-info badge-pill">view more</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <span class=" p-o-percentage mr-4">60%</span>
                                        <div class="progress p-o-progress mt-2">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                 style="width: 60%" aria-valuenow="60" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <h6>Doctors</h6>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <span class="badge badge-info">IN PROGRESS</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}"
                                                     class="ml-0">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item badge-notify mr-0">
                                                <div class="notification">
                                                    <a href="/support/ChatView"><span
                                                                class="badge badge-info badge-pill">view more</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <span class=" p-o-percentage mr-4">60%</span>
                                        <div class="progress p-o-progress mt-2">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                 style="width: 60%" aria-valuenow="60" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <h6>Accounting</h6>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <span class="badge badge-info">IN PROGRESS</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}"
                                                     class="ml-0">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item badge-notify mr-0">
                                                <div class="notification">
                                                    <a href="/support/ChatView"><span
                                                                class="badge badge-info badge-pill">view more</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <span class=" p-o-percentage mr-4">60%</span>
                                        <div class="progress p-o-progress mt-2">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                 style="width: 60%" aria-valuenow="60" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <h6>Sales</h6>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <span class="badge badge-info">IN PROGRESS</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}"
                                                     class="ml-0">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item badge-notify mr-0">
                                                <div class="notification">
                                                    <a href="/support/ChatView"><span
                                                                class="badge badge-info badge-pill">view more</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <span class=" p-o-percentage mr-4">60%</span>
                                        <div class="progress p-o-progress mt-2">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                 style="width: 60%" aria-valuenow="60" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="progress-order">
                            <div class="progress-order-header">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-12">
                                        <h6>Receptionist</h6>
                                    </div>
                                    <div class="col-md-6 pl-0 col-sm-6 col-12 text-right">
                                        <span class="badge badge-info">IN PROGRESS</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress-order-body">
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <ul class="list-inline badge-collapsed-img mb-0 mb-3">
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}"
                                                     class="ml-0">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item chat-online-usr">
                                                <img alt="avatar" src="{{asset('storage/img/90x90.jpg')}}">
                                            </li>
                                            <li class="list-inline-item badge-notify mr-0">
                                                <div class="notification">
                                                    <a href="/support/ChatView"><span
                                                                class="badge badge-info badge-pill">view more</span></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <span class=" p-o-percentage mr-4">60%</span>
                                        <div class="progress p-o-progress mt-2">
                                            <div class="progress-bar bg-primary" role="progressbar"
                                                 style="width: 60%" aria-valuenow="60" aria-valuemin="0"
                                                 aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection