@extends('layouts.app')

@section('content')

    <style>
        .alert-danger{
            margin-left: 18px;
            margin-right: 21px;
            margin-top: 15px;

            color: #f7f9fd;
            background-color: #0e1726;
            border-color: #fff;
        }
         .alert-success{
             margin-left: 18px;
             margin-right: 21px;
             margin-top: 15px;
         }
    </style>

    @if (count($errors) > 0)
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-dismissable alert-success" style="margin-top:10px">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @endif
    <div class="layout-px-spacing">
        <div class="row layout-spacing">
            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-4 col-lg-5 col-md-5 col-sm-7 filtered-list-search layout-spacing align-self-center">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>Client Datatable</h4>
                                </div>
                            </div>

                            <div class="col-xl-8 col-lg-7 col-md-7 col-sm-5 text-sm-right text-center layout-spacing align-self-center">
                                <div class="d-flex justify-content-sm-end justify-content-center" style="cursor: pointer">
                                    <svg id="btn-add-contact" xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                         stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-user-plus">
                                        <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="8.5" cy="7" r="4"></circle>
                                        <line x1="20" y1="8" x2="20" y2="14"></line>
                                        <line x1="23" y1="11" x2="17" y2="11"></line>
                                    </svg>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="addContactModal" tabindex="-1" role="dialog"
                                     aria-labelledby="addContactModalTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            {{--<div class="modal-header">--}}
                                                {{--Add New Client--}}
                                            {{--</div>--}}
                                            <div class="modal-body">
                                                <i class="flaticon-cancel-12 close" data-dismiss="modal"></i>
                                                <div class="add-contact-box">
                                                    <div class="add-contact-content">
                                                            {!! Form::open(array('route' => 'admin.Client.store','method'=>'POST')) !!}
                                                            <div class="row">
                                                                <div class="col-md-6 mt-4">
                                                                    <div class="contact-name">
                                                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6  mt-4">
                                                                    <div class="contact-email">
                                                                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6  mt-4">
                                                                    <div class="contact-password">
                                                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6  mt-4">
                                                                    <div class="confirm-password">
                                                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        {{--@php--}}
                                                            {{--if($roles[0] == "Client")--}}
                                                        {{--{--}}
                                                         {{--$roles = 'Client';--}}
                                                        {{--}--}}
                                                        {{--@endphp--}}

                                                            <div class="row">
                                                                <div class="col-md-12  mt-4">
                                                                    <div class="roles">
                                                                        {!! Form::text('roles[]', 'Client' , array('class' => 'form-control','hidden')) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <style>
                                                                .form-control{
                                                                    color: #687372;

                                                                }
                                                                .form-control::after{
                                                                    color: #687372;
                                                                }
                                                            </style>
                                                            <button style="background: #1b2e4b; color: white;" class="btn mt-4" data-dismiss="modal"><i
                                                                        class="flaticon-delete-1"></i> Discard
                                                            </button>

                                                            <button type="submit" style="background: #1b55e2; color: white;" id="btn-add" class="btn mt-4">Add</button>
                                                            {!! Form::close() !!}
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="widget-content widget-content-area">
                            <div class="table-responsive mb-4">
                                <table id="individual-col-search" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Status</th>
                                        <th>Appoinment</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td><span class="shadow-none badge badge-primary">Approved</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="profile" class="btn btn-primary">
                                            Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td>Garrett Winters</td>
                                        <td>Accountant</td>
                                        <td>Tokyo</td>
                                        <td>63</td>
                                        <td><span class="shadow-none badge badge-primary">Approved</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td>Ashton Cox</td>
                                        <td>Junior Technical Author</td>
                                        <td>San Francisco</td>
                                        <td>66</td>
                                        <td><span class="shadow-none badge badge-primary">Approved</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td>Cedric Kelly</td>
                                        <td>Senior Javascript Developer</td>
                                        <td>Edinburgh</td>
                                        <td>22</td>
                                        <td><span class="shadow-none badge badge-primary">Approved</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td>Airi Satou</td>
                                        <td>Accountant</td>
                                        <td>Tokyo</td>
                                        <td>33</td>
                                        <td><span class="shadow-none badge badge-primary">Approved</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td>Brielle Williamson</td>
                                        <td>Integration Specialist</td>
                                        <td>New York</td>
                                        <td>61</td>
                                        <td><span class="shadow-none badge badge-warning">Pending</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td>Herrod Chandler</td>
                                        <td>Sales Assistant</td>
                                        <td>San Francisco</td>
                                        <td>59</td>
                                        <td><span class="shadow-none badge badge-danger">Suspended</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">8</td>
                                        <td>Rhona Davidson</td>
                                        <td>Integration Specialist</td>
                                        <td>Tokyo</td>
                                        <td>55</td>
                                        <td><span class="shadow-none badge badge-warning">Pending</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">9</td>
                                        <td>Colleen Hurst</td>
                                        <td>Javascript Developer</td>
                                        <td>San Francisco</td>
                                        <td>39</td>
                                        <td><span class="shadow-none badge badge-danger">Suspended</span></td>
                                        <td>
                                            <div class="progress br-30">
                                                <div class="progress-bar br-30 bg-warning" role="progressbar"
                                                     style="width: 67%" aria-valuenow="67" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">10</td>
                                        <td>Sonya Frost</td>
                                        <td>Software Engineer</td>
                                        <td>Edinburgh</td>
                                        <td>23</td>
                                        <td>2013/03/03</td>
                                        <td>2008/12/13</td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">11</td>
                                        <td>Jena Gaines</td>
                                        <td>Office Manager</td>
                                        <td>London</td>
                                        <td>30</td>
                                        <td>2008/12/19</td>
                                        <td>$90,560</td>
                                        <td class="text-center"><a href="" class="btn btn-primary">
                                                Edit</a></td>
                                    </tr>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                        <th class="invisible"></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection