@extends('layouts.app')

@section('content')
    <style>
        .widget-account-invoice-two .account-box .info {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6px;
        }

        .widget-account-invoice-two .account-box .acc-action {
            margin-top: 0px;
            display: flex;
            justify-content: space-between;
            margin-bottom: 45px;
        }

        #avatars {
            background: linear-gradient(to left, #2b7748 0%, #2f384f 100%);
        }

        .btn-primary {
            background-color: #18a96d !important;;
            border-color: #18a96d !important;;
        }
    </style>
    <div class="layout-px-spacing">

        <div class="row layout-spacing">

            <!-- Content -->
            <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">

                <div class="user-profile layout-spacing">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">Profile</h3>
                            <a href="/users/account_settings" class="mt-2 edit-profile">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-edit-3">
                                    <path d="M12 20h9"></path>
                                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                </svg>
                            </a>
                        </div>
                        <div class="text-center user-info">
                            <img src="{{asset('storage/img/90x90.jpg')}}" alt="avatar">
                            <p class="">Jimmy Turner</p>
                        </div>
                        <div class="user-info-list">

                            <div class="">
                                <ul class="contacts-block list-unstyled">
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-coffee">
                                            <path d="M18 8h1a4 4 0 0 1 0 8h-1"></path>
                                            <path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path>
                                            <line x1="6" y1="1" x2="6" y2="4"></line>
                                            <line x1="10" y1="1" x2="10" y2="4"></line>
                                            <line x1="14" y1="1" x2="14" y2="4"></line>
                                        </svg>
                                        Web Developer
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-calendar">
                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                        </svg>
                                        Jan 20, 1989
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-map-pin">
                                            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                                            <circle cx="12" cy="10" r="3"></circle>
                                        </svg>
                                        New York, USA
                                    </li>
                                    <li class="contacts-block__item">
                                        <a href="mailto:example@mail.com">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-mail">
                                                <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                                <polyline points="22,6 12,13 2,6"></polyline>
                                            </svg>
                                            Jimmy@gmail.com</a>
                                    </li>
                                    <li class="contacts-block__item">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-phone">
                                            <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                        </svg>
                                        +1 (530) 555-12121
                                    </li>
                                    <li class="contacts-block__item">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <div class="social-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-facebook">
                                                        <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="list-inline-item">
                                                <div class="social-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-twitter">
                                                        <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
                                                    </svg>
                                                </div>
                                            </li>
                                            <li class="list-inline-item">
                                                <div class="social-icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-linkedin">
                                                        <path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path>
                                                        <rect x="2" y="9" width="4" height="12"></rect>
                                                        <circle cx="4" cy="4" r="2"></circle>
                                                    </svg>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                <div class="bio layout-spacing ">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">Profile</h3>
                            <a href="" style="height: 38px;" class="btn btn-primary">Add</a>
                        </div>

                        <div class="row">
                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Old Files</h5>
                                            </div>
                                            <p class="inv-balance">3.5GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Avatars</h5>
                                            </div>
                                            <p class="inv-balance">450 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Travel Photo</h5>
                                            </div>
                                            <p class="inv-balance">2.2GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Profile</h5>
                                            </div>
                                            <p class="inv-balance">670 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget-content widget-content-area mt-1">
                            <div class="d-flex justify-content-between">
                                <h3 class="">Document</h3>
                                <a href="" style="height: 38px;" class="btn btn-primary">Add</a>
                            </div>

                        <div class="row">
                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Old Files</h5>
                                            </div>
                                            <p class="inv-balance">3.5GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Avatars</h5>
                                            </div>
                                            <p class="inv-balance">450 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Travel Photo</h5>
                                            </div>
                                            <p class="inv-balance">2.2GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Profile</h5>
                                            </div>
                                            <p class="inv-balance">670 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <div class="d-flex justify-content-between mt-2">
                                    <h4 class="">To Do</h4>
                                    <a href="" style="height: 38px;" class="btn btn-primary">Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content widget-content-area">
                        <div id="example-vertical">
                            <h3>Keyboard</h3>
                            <section>
                                <p>Try the keyboard navigation by clicking arrow left or right!</p>
                            </section>
                            <h3>Effects</h3>
                            <section>
                                <p>Wonderful transition effects.</p>
                            </section>
                            <h3>Pager</h3>
                            <section>
                                <p>The next and previous buttons help you to navigate through your content.</p>
                            </section>
                        </div>

                        <div class="code-section-container">

                            <button class="btn toggle-code-snippet"><span>Code</span></button>

                            <div class="code-section text-left">
                                            <pre>
$("selector").steps({
    cssClass: 'circle wizard',
    stepsOrientation: "vertical"
});
</pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-7 col-sm-12  layout-spacing">
                <div class="work-experience layout-spacing ">

                    <div class="widget-content widget-content-area">

                        <h3 class="">Timeline</h3>

                        <div class="timeline-alter">

                            <div class="item-timeline">
                                <div class="t-meta-date">
                                    <p class="">04 Mar 2009</p>
                                </div>
                                <div class="t-dot">
                                </div>
                                <div class="t-text">
                                    <p>Netfilx Inc.</p>
                                    <p>Designer Illustrator</p>
                                </div>
                            </div>

                            <div class="item-timeline">
                                <div class="t-meta-date">
                                    <p class="">25 Apr 2014</p>
                                </div>
                                <div class="t-dot">
                                </div>
                                <div class="t-text">
                                    <p>Google Inc.</p>
                                    <p>Designer Illustrator</p>
                                </div>
                            </div>

                            <div class="item-timeline">
                                <div class="t-meta-date">
                                    <p class="">04 Apr 2018</p>
                                </div>
                                <div class="t-dot">
                                </div>
                                <div class="t-text">
                                    <p>Design Reset Inc.</p>
                                    <p>Designer Illustrator</p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection