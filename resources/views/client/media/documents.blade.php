@extends('layouts.app')

@section('content')
    <style>
        .widget-account-invoice-two .account-box .info {
            display: flex;
            justify-content: space-between;
            margin-bottom: 6px;
        }

        .widget-account-invoice-two .account-box .acc-action {
            margin-top: 0px;
            display: flex;
            justify-content: space-between;
            margin-bottom: 45px;
        }

        #avatars {
            background: linear-gradient(to left, #2b7748 0%, #2f384f 100%);
        }

        .btn-primary {
            background-color: #18a96d !important;;
            border-color: #18a96d !important;;
        }
    </style>
    <div class="layout-px-spacing">

        <div class="row layout-spacing">

            <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
                <div class="bio layout-spacing ">
                    <div class="widget-content widget-content-area">
                        <div class="d-flex justify-content-between">
                            <h3 class="">Documents</h3>
                            <a href="" style="height: 38px;" class="btn btn-primary">Add</a>
                        </div>

                        <div class="row">
                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Old Files</h5>
                                            </div>
                                            <p class="inv-balance">3.5GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Avatars</h5>
                                            </div>
                                            <p class="inv-balance">450 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round"
                                                             class="feather feather-credit-card">
                                                            <rect x="1" y="4" width="22" height="16" rx="2"
                                                                  ry="2"></rect>
                                                            <line x1="1" y1="10" x2="23" y2="10"></line>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Travel Photo</h5>
                                            </div>
                                            <p class="inv-balance">2.2GB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two" id="avatars">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="acc-action">
                                                <div class="">
                                                    <a href="javascript:void(0);">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-camera">
                                                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                                            <circle cx="12" cy="13" r="4"></circle>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="info">
                                                <h5 class="">Profile</h5>
                                            </div>
                                            <p class="inv-balance">670 KB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>


        </div>
    </div>


@endsection