@extends('layouts.app')

@section('content')

    <div class="layout-px-spacing">
        <div class="account-settings-container layout-top-spacing">
            <div class="account-content">
                <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                            <form id="general-info" class="section general-info">
                                <div class="info">
                                    <h6 class="">General Information</h6>
                                    <div class="row">
                                        <div class="col-lg-11 mx-auto">
                                            <div class="row">
                                                <div class="col-xl-2 col-lg-12 col-md-4">
                                                    <div class="upload mt-4 pr-md-4">
                                                        <input type="file" id="input-file-max-fs" class="dropify" data-default-file="{{asset('storage/img/200x200.jpg')}}" data-max-file-size="2M" />
                                                        <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Picture</p>
                                                    </div>
                                                </div>

                                                <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                    <div class="form">
                                                        <div class="form-group">
                                                            <label for="profession">Email :</label>
                                                            <label id="profession">{{$doctor->email}}</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="profession">First name :</label>
                                                            <input type="text" class="form-control mb-4" id="profession" placeholder="Designer" value="{{$doctor->name}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="profession">Last name :</label>
                                                            <input type="text" class="form-control mb-4" id="profession" placeholder="Designer" value="{{$doctor->surname}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 p-0">
                <form id="work-experience" class="section work-experience">
                    <div class="info">
                        <h5 class="">Services</h5>
                        <div class="row">

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
