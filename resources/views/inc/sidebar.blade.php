@if ($page_name != 'coming_soon' && $page_name != 'contact_us' && $page_name != 'error404' && $page_name != 'error500' && $page_name != 'error503' && $page_name != 'faq' && $page_name != 'helpdesk' && $page_name != 'maintenence' && $page_name != 'privacy' && $page_name != 'auth_boxed' && $page_name != 'auth_default')

    <!--  BEGIN SIDEBAR  -->
    <div class="sidebar-wrapper sidebar-theme">

        <nav id="sidebar">
            <div class="shadow-bottom"></div>

            <ul class="list-unstyled menu-categories" id="accordionExample">
                @can('client')
                    <li class="menu {{ ($category_name === 'dashboard') ? 'active' : '' }}">
                        <a href="client/analytics" data-active="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                <span>Dashboard</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'dashboard') ? 'show' : '' }}"--}}
                        {{--id="dashboard" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'sales') ? 'active' : '' }}">--}}
                        {{--<a href="/sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'analytics') ? 'active' : '' }}">--}}
                        {{--<a href="/analytics"> Analytics </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'appointment') ? 'active' : '' }}">
                        <a href="#Appointments"
                           data-active="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-check">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <polyline points="17 11 19 13 23 9"></polyline>
                                </svg>
                                <span>Appointments</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'appointment' || $category_name === 'Video_Call_Appointments') ? 'show' : '' }}"
                            id="Appointments" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'calendar') ? 'active' : '' }}">
                                <a href="/client/calendar"> Appointment Calender  </a>
                            </li>
                            <li class="{{ ($page_name === 'Video_Call_Appointments') ? 'active' : '' }}">
                                <a href="/client/Video_Call_Appointments"> Video Call Appointments </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Invoice') ? 'active' : '' }}">
                        <a href="#Invoice" data-active="{{ ($category_name === 'Invoice') ? 'true' : 'false' }}"
                           data-toggle="collapse" aria-expanded="{{ ($category_name === 'Invoice') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-camera">
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                    <circle cx="12" cy="13" r="4"></circle>
                                </svg>
                                <span>Invoice</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Invoice') ? 'show' : '' }}"
                            id="Invoice" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Invoice Info') ? 'active' : '' }}">
                                <a href="/client/Invoice_Info"> Invoice Info </a>
                            </li>

                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Full_History') ? 'active' : '' }}">
                        <a href="/client/Full_History" data-active="{{ ($category_name === 'Full_History') ? 'true' : 'false' }}"
                           data-toggle="" aria-expanded="{{ ($category_name === 'Full_History') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-camera">
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                    <circle cx="12" cy="13" r="4"></circle>
                                </svg>
                                <span>Full History</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'Invoice') ? 'show' : '' }}"--}}
                        {{--id="Invoice" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'Invoice Info') ? 'active' : '' }}">--}}
                        {{--<a href="/Invoice/Invoice_Info"> Invoice Info </a>--}}
                        {{--</li>--}}

                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Accounting"> Accounting </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'To Do') ? 'active' : '' }}">
                        <a href="#To_Do_List" data-active="{{ ($category_name === 'To do') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'To Do') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-folder-plus">
                                    <path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path>
                                    <line x1="12" y1="11" x2="12" y2="17"></line>
                                    <line x1="9" y1="14" x2="15" y2="14"></line>
                                </svg>
                                <span>To Do</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'To Do') ? 'show' : '' }}" id="To_Do_List" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'To Do List') ? 'active' : '' }}">
                                <a href="/client/To_Do_List"> To Do List </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Doctors"> Doctors </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Media') ? 'active' : '' }}">
                        <a href="#Media" data-active="{{ ($category_name === 'Media') ? 'true' : 'false' }}"
                           data-toggle="collapse" aria-expanded="{{ ($category_name === 'Media') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-camera">
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                    <circle cx="12" cy="13" r="4"></circle>
                                </svg>
                                <span>Media</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Media') ? 'show' : '' }}"
                            id="Media" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Video') ? 'active' : '' }}">
                                <a href="/client/Video"> Videos </a>
                            </li>
                            <li class="{{ ($page_name === 'Documents') ? 'active' : '' }}">
                                <a href="/client/Documents"> Documents </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'support') ? 'active' : '' }}">
                        <a href="/client/Support" data-active="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-phone-forwarded">
                                    <polyline points="19 1 23 5 19 9"></polyline>
                                    <line x1="15" y1="5" x2="23" y2="5"></line>
                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                </svg>
                                <span>Support</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'support') ? 'show' : '' }}"--}}
                        {{--id="Support" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'Client') ? 'active' : '' }}">--}}
                        {{--<a href="/support/Client"> Client </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'Doctors') ? 'active' : '' }}">--}}
                        {{--<a href="/support/Client"> Doctors </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/support/Client"> Accounting </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/support/Client"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/support/Client"> Receptionist </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'Profile') ? 'active' : '' }}">
                        <a href="/client/Profile" data-active="{{ ($category_name === 'Profile') ? 'true' : 'false' }}"
                           data-toggle="" aria-expanded="{{ ($category_name === 'Profile') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-camera">
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                    <circle cx="12" cy="13" r="4"></circle>
                                </svg>
                                <span>Profile</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'Media') ? 'show' : '' }}"--}}
                        {{--id="Profile" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'Video') ? 'active' : '' }}">--}}
                        {{--<a href="/Media/Video"> Videos </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Accounting"> Accounting </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>


                    <li class="menu {{ ($category_name === 'Logout') ? 'active' : '' }}">
                        <a href="{{ route('logout') }}"
                           data-active="{{ ($category_name === 'Logout') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Logout') ? 'true' : 'false' }}"
                           class="dropdown-toggle" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-lock">
                                    <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                    <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                </svg>
                                <span>Logout</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                    </li>
                    @endcan
                @can('receptionist')
                    <li class="menu {{ ($category_name === 'dashboard') ? 'active' : '' }}">
                        <a href="/receptionist/receptionist_dashboard" data-active="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                <span>Dashboard</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'dashboard') ? 'show' : '' }}"--}}
                        {{--id="dashboard" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'sales') ? 'active' : '' }}">--}}
                        {{--<a href="/sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'analytics') ? 'active' : '' }}">--}}
                        {{--<a href="/analytics"> Analytics </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'appointment') ? 'active' : '' }}">
                        <a href="#Appointments"
                           data-active="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-check">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <polyline points="17 11 19 13 23 9"></polyline>
                                </svg>
                                <span>Appointments</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Appointment Calendar' || $category_name === 'Video_Call_Appointments') ? 'show' : '' }}"
                            id="Appointments" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Appointment Calendar') ? 'active' : '' }}">
                                <a href="/receptionist/Appointment_Calendar">Appointment Calendar </a>
                            </li>
                            <li class="{{ ($page_name === 'Video_Call_Appointments') ? 'active' : '' }}">
                                <a href="/receptionist/Video_Call_Appointments">Live Video Appointments </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'support') ? 'active' : '' }}">
                        <a href="#Support" data-active="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-phone-forwarded">
                                    <polyline points="19 1 23 5 19 9"></polyline>
                                    <line x1="15" y1="5" x2="23" y2="5"></line>
                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                </svg>
                                <span>Support</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'receptionist_support') ? 'show' : '' }}"
                            id="Support" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'receptionist_support') ? 'active' : '' }}">
                                <a href="/receptionist/receptionist_support"> Support Chat </a>
                            </li>
                        </ul>
                    </li>

                @endcan
                @can('doctor')
                    <li class="menu {{ ($category_name === 'dashboard') ? 'active' : '' }}">
                        <a href="/sales" data-active="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                <span>Dashboard</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'dashboard') ? 'show' : '' }}"--}}
                        {{--id="dashboard" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'sales') ? 'active' : '' }}">--}}
                        {{--<a href="/sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'analytics') ? 'active' : '' }}">--}}
                        {{--<a href="/analytics"> Analytics </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'appointment') ? 'active' : '' }}">
                        <a href="#Appointments"
                           data-active="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-check">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <polyline points="17 11 19 13 23 9"></polyline>
                                </svg>
                                <span>Appointments</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'appointment' || $category_name === 'Video_Call_Appointments') ? 'show' : '' }}"
                            id="Appointments" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Video_Call_Appointments') ? 'active' : '' }}">
                                <a href="/doctor/Video_Call_Appointments">Live Video Appointments </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'users') ? 'active' : '' }}">
                        <a href="#users" data-active="{{ ($category_name === 'users') ? 'true' : 'false' }}"
                           data-toggle="collapse" aria-expanded="{{ ($category_name === 'users') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-users">
                                    <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="9" cy="7" r="4"></circle>
                                    <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                </svg>
                                <span>Users</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'users') ? 'show' : '' }}"
                            id="users" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Clients') ? 'active' : '' }}">
                                <a href="/users/Clients"> All Users </a>
                            </li>
                        </ul>
                    </li>

                    @endcan


                @can('user-management')
                @if ($page_name != 'alt_menu' && $page_name != 'blank_page' && $page_name != 'boxed' && $page_name != 'breadcrumb' )

                    <li class="menu {{ ($category_name === 'dashboard') ? 'active' : '' }}">
                        <a href="/sales" data-active="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'dashboard') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                <span>Dashboard</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'dashboard') ? 'show' : '' }}"--}}
                        {{--id="dashboard" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'sales') ? 'active' : '' }}">--}}
                        {{--<a href="/sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'analytics') ? 'active' : '' }}">--}}
                        {{--<a href="/analytics"> Analytics </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'users') ? 'active' : '' }}">
                        <a href="#users" data-active="{{ ($category_name === 'users') ? 'true' : 'false' }}"
                           data-toggle="collapse" aria-expanded="{{ ($category_name === 'users') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-users">
                                    <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="9" cy="7" r="4"></circle>
                                    <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                </svg>
                                <span>Users</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'users') ? 'show' : '' }}"
                            id="users" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Clients') ? 'active' : '' }}">
                                <a href="/users/Clients"> Clients </a>
                            </li>
                            <li class="{{ ($page_name === 'Doctors') ? 'active' : '' }}">
                                <a href="/users/Clients"> Doctors </a>
                            </li>
                            <li class="{{ ($page_name === 'Accounting') ? 'active' : '' }}">
                                <a href="/users/Clients"> Accounting </a>
                            </li>
                            <li class="{{ ($page_name === 'Sales') ? 'active' : '' }}">
                                <a href="/users/Clients"> Sales </a>
                            </li>
                            <li class="{{ ($page_name === 'Receptionist') ? 'active' : '' }}">
                                <a href="/users/Clients"> Receptionist </a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Contact') ? 'active' : '' }}">
                        <a href="/contact" data-active="{{ ($category_name === 'Contact') ? 'true' : 'false' }}"
                           data-toggle=""
                           aria-expanded="{{ ($category_name === 'Contact') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-plus">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <line x1="20" y1="8" x2="20" y2="14"></line>
                                    <line x1="23" y1="11" x2="17" y2="11"></line>
                                </svg>
                                <span>Contacts</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'users') ? 'show' : '' }}" id="users" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'profile') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Client"> Client </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Doctors"> Doctors </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Accounting"> Accounting </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'finance') ? 'active' : '' }}">
                        <a href="#finance" data-active="{{ ($category_name === 'finance') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'finance') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-dollar-sign">
                                    <line x1="12" y1="1" x2="12" y2="23"/>
                                    <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"/>
                                </svg>
                                <span>Finance</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'finance') ? 'show' : '' }}"
                            id="finance" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'finance') ? 'active' : '' }}">
                                <a href="/finance/finance">Finance Detail List </a>
                            </li>
                            <li class="{{ ($page_name === 'finance_drag_and_drop') ? 'active' : '' }}">
                                <a href="/finance/finance_drag_and_drop"> Finance 2 </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'appointment') ? 'active' : '' }}">
                        <a href="#Appointments"
                           data-active="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'appointment') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-check">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <polyline points="17 11 19 13 23 9"></polyline>
                                </svg>
                                <span>Appointments</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'appointment' || $category_name === 'Video_Call_Appointments') ? 'show' : '' }}"
                            id="Appointments" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'appointment') ? 'active' : '' }}">
                                <a href="/appointment/appointment"> Appointment List </a>
                            </li>
                            <li class="{{ ($page_name === 'Video_Call_Appointments') ? 'active' : '' }}">
                                <a href="/appointment/Video_Call_Appointments"> Video Call Appointments </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Case_Files') ? 'active' : '' }}">
                        <a href="#Case_Files" data-active="{{ ($category_name === 'Case_Files') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Case_Files') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-folder-plus">
                                    <path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path>
                                    <line x1="12" y1="11" x2="12" y2="17"></line>
                                    <line x1="9" y1="14" x2="15" y2="14"></line>
                                </svg>
                                <span>Case Files</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        {{--<ul class="collapse submenu list-unstyled {{ ($category_name === 'users') ? 'show' : '' }}" id="users" data-parent="#accordionExample">--}}
                        {{--<li class="{{ ($page_name === 'profile') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Client"> Client </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Doctors"> Doctors </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Accounting"> Accounting </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Sales"> Sales </a>--}}
                        {{--</li>--}}
                        {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                        {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </li>

                    <li class="menu {{ ($category_name === 'Media') ? 'active' : '' }}">
                        <a href="#Media" data-active="{{ ($category_name === 'Media') ? 'true' : 'false' }}"
                           data-toggle="collapse" aria-expanded="{{ ($category_name === 'Media') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-camera">
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                                    <circle cx="12" cy="13" r="4"></circle>
                                </svg>
                                <span>Media</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Media') ? 'show' : '' }}"
                            id="Media" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Video') ? 'active' : '' }}">
                                <a href="/Media/Video"> Videos </a>
                            </li>
                            <li class="{{ ($page_name === 'Documents') ? 'active' : '' }}">
                                <a href="/Media/Documents"> Documents </a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'support') ? 'active' : '' }}">
                        <a href="#Support" data-active="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'support') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-phone-forwarded">
                                    <polyline points="19 1 23 5 19 9"></polyline>
                                    <line x1="15" y1="5" x2="23" y2="5"></line>
                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                </svg>
                                <span>Support</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'support') ? 'show' : '' }}"
                            id="Support" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Client') ? 'active' : '' }}">
                                <a href="/support/Client"> Client </a>
                            </li>
                            <li class="{{ ($page_name === 'Doctors') ? 'active' : '' }}">
                                <a href="/support/Client"> Doctors </a>
                            </li>
                            <li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">
                                <a href="/support/Client"> Accounting </a>
                            </li>
                            <li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">
                                <a href="/support/Client"> Sales </a>
                            </li>
                            <li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">
                                <a href="/support/Client"> Receptionist </a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Deals') ? 'active' : '' }}">
                        <a href="#Deals" data-active="{{ ($category_name === 'Finance') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Deals') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-dollar-sign">
                                    <line x1="12" y1="1" x2="12" y2="23"/>
                                    <path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"/>
                                </svg>
                                <span>Deals</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Deals') ? 'show' : '' }}"
                            id="Deals" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Deals') ? 'active' : '' }}">
                                <a href="/Deals/Deals">Deals</a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Services') ? 'active' : '' }}">
                        <a href="#Service" data-active="{{ ($category_name === 'Services') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Deals') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-check-circle">
                                    <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                                    <polyline points="22 4 12 14.01 9 11.01"></polyline>
                                </svg>
                                <span>Services</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Services') ? 'show' : '' }}"
                            id="Service" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Services') ? 'active' : '' }}">
                                <a href="/Services">Services</a>
                            </li>
                            <li class="{{ ($page_name === 'Add Services') ? 'active' : '' }}">
                                <a href="/add_services">Add Services</a>
                            </li>
                            {{--<li class="{{ ($page_name === 'Add Addons') ? 'active' : '' }}">--}}
                                {{--<a href="/add_addons">Add Addons</a>--}}
                            {{--</li>--}}
                            <li class="{{ ($page_name === 'Doctors') ? 'active' : '' }}">
                                <a href="/doctors">Doctors</a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Clinics') ? 'active' : '' }}">
                        <a href="#Clinics" data-active="{{ ($category_name === 'Clinics') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Clinics') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-activity">
                                    <polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline>
                                </svg>
                                <span>Clinics</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Clinics') ? 'show' : '' }}"
                            id="Clinics" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Clinics') ? 'active' : '' }}">
                                <a href="/Clinics/Clinics">Clinics Data</a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Contracts') ? 'active' : '' }}">
                        <a href="#Contracts" data-active="{{ ($category_name === 'Contracts') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Contracts') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-user-plus">
                                    <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                    <circle cx="8.5" cy="7" r="4"></circle>
                                    <line x1="20" y1="8" x2="20" y2="14"></line>
                                    <line x1="23" y1="11" x2="17" y2="11"></line>
                                </svg>
                                <span>Contracts</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'Contracts') ? 'show' : '' }}"
                            id="Contracts" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'Contracts') ? 'active' : '' }}">
                                <a href="/Contracts/Contracts">Contracts Data</a>
                            </li>
                            <li class="{{ ($page_name === 'Contracts') ? 'active' : '' }}">
                                <a href="/Contracts/add_new_invoice">Add new invoice</a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'maps') ? 'active' : '' }}">
                        <a href="#Maps" data-active="{{ ($category_name === 'maps') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'maps') ? 'true' : 'false' }}"
                           class="dropdown-toggle">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-map-pin">
                                    <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                                    <circle cx="12" cy="10" r="3"></circle>
                                </svg>
                                <span>Maps</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled {{ ($category_name === 'maps') ? 'show' : '' }}"
                            id="Maps" data-parent="#accordionExample">
                            <li class="{{ ($page_name === 'maps') ? 'active' : '' }}">
                                <a href="/Maps/UK">United Kingdom</a>
                            </li>
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Accounting"> Accounting </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Sales"> Sales </a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                            {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>

                    <li class="menu {{ ($category_name === 'Logout') ? 'active' : '' }}">
                        <a href="{{ route('logout') }}"
                           data-active="{{ ($category_name === 'Logout') ? 'true' : 'false' }}"
                           data-toggle="collapse"
                           aria-expanded="{{ ($category_name === 'Logout') ? 'true' : 'false' }}"
                           class="dropdown-toggle" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-lock">
                                    <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                    <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                </svg>
                                <span>Logout</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                        </a>
                    </li>





                            <li class="menu {{ ($category_name === 'User Management') ? 'active' : '' }}">
                                <a href="#User_Management" data-active="{{ ($category_name === 'User Management') ? 'true' : 'false' }}"
                                   data-toggle="collapse"
                                   aria-expanded="{{ ($category_name === 'User Management') ? 'true' : 'false' }}"
                                   class="dropdown-toggle">
                                    <div class="">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-map-pin">
                                            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                                            <circle cx="12" cy="10" r="3"></circle>
                                        </svg>
                                        <span>User Management</span>
                                    </div>
                                    <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round" class="feather feather-chevron-right">
                                            <polyline points="9 18 15 12 9 6"></polyline>
                                        </svg>
                                    </div>
                                </a>
                                <ul class="collapse submenu list-unstyled {{ ($category_name === 'User Management') ? 'show' : '' }}"
                                    id="User_Management" data-parent="#accordionExample">
                                    <li class="{{ ($page_name === 'Users') ? 'active' : '' }}">
                                        <a href="{{route('admin.user.index')}}">Users</a>
                                    </li>
                                    <li class="{{ ($page_name === 'Roles') ? 'active' : '' }}">
                                        <a href="{{route('admin.role.index')}}">Roles</a>
                                    </li>
                                    {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                                    {{--<a href="/users/Accounting"> Accounting </a>--}}
                                    {{--</li>--}}
                                    {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                                    {{--<a href="/users/Sales"> Sales </a>--}}
                                    {{--</li>--}}
                                    {{--<li class="{{ ($page_name === 'account_setting') ? 'active' : '' }}">--}}
                                    {{--<a href="/users/Receptionist"> Receptionist </a>--}}
                                    {{--</li>--}}
                                </ul>
                            </li>


                    @endcan

                @endif


            </ul>

        </nav>

    </div>
    <!--  END SIDEBAR  -->

@endif
